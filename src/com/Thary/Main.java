package com.Thary;

import org.nocrala.tools.texttablefmt.BorderStyle;
import org.nocrala.tools.texttablefmt.CellStyle;
import org.nocrala.tools.texttablefmt.ShownBorders;
import org.nocrala.tools.texttablefmt.Table;
import java.io.Serializable;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.*;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Scanner;

 public class Main implements Serializable {
    static Scanner Input = new Scanner(System.in);
    static ArrayList<Product> ProductList = new ArrayList<>();
    static int proID ;
    static String proName;
    static double proUnit ;
    static int proStock_Q;
    static String importedDate;
     public static CellStyle center = new CellStyle(CellStyle.HorizontalAlign.center); // Table Alignment
    public Main(int id, String name, double unitPrice, int stock_Q, String importedDate) {
    }
     public Main() {
     
     }
     Product proObj = new Product(proID,proName,proUnit,proStock_Q,importedDate){};
     public String dateImported () {
         DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
         LocalDateTime now = LocalDateTime.now();
         return dtf.format(now);
     }
     public void groupLogo (){
         int width = 100;
         int height = 18;
         BufferedImage image = new BufferedImage(width, height, 1);
         Graphics g = image.getGraphics();
         g.setFont(new Font("SansSerif", 1, 18));
         Graphics2D graphics = (Graphics2D)g;
         graphics.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
         graphics.drawString("BTB G 4", 14, 15);
    
         for(int y = 0; y < height; ++y) {
             StringBuilder sb = new StringBuilder();
        
             for(int x = 0; x < width; ++x) {
                 sb.append(image.getRGB(x, y) == -16777216 ? " " : "#");
             }
        
             if (!sb.toString().trim().isEmpty()) {
                 System.out.println(sb);
             }
         }
    }
    public void menuOption (){
        
        CellStyle center = new CellStyle(CellStyle.HorizontalAlign.center);
        Table menuTable = new Table(9, BorderStyle.UNICODE_BOX_DOUBLE_BORDER_WIDE,ShownBorders.SURROUND);

        menuTable.setColumnWidth(0,15,50);
        menuTable.setColumnWidth(1,15,50);
        menuTable.setColumnWidth(2,15,50);
        menuTable.setColumnWidth(3,15,50);
        menuTable.setColumnWidth(4,15,50);
        menuTable.setColumnWidth(5,15,50);
        menuTable.setColumnWidth(6,15,50);
        menuTable.setColumnWidth(7,15,50);
        menuTable.setColumnWidth(8,15,50);

        menuTable.addCell("*) Display",center);
        menuTable.addCell("| W) Write",center);
        menuTable.addCell("| R) Read",center);
        menuTable.addCell("| U) Update",center);
        menuTable.addCell("| D) Delete",center);
        menuTable.addCell("| F) First",center);
        menuTable.addCell("| P) Previous",center);
        menuTable.addCell("| N) Next",center);
        menuTable.addCell("| N) Last",center);
    
    
            menuTable.addCell("S) Search",center);
            menuTable.addCell("| G) Goto",center);
            menuTable.addCell("| Se) Set Row",center);
            menuTable.addCell("| Sa) Save",center);
            menuTable.addCell("| Ba) Back up",center);
            menuTable.addCell("| Re) Restore",center);
            menuTable.addCell("| H) Help",center);
            menuTable.addCell("| E) Exit",center);
    
            System.out.println(menuTable.render());
    }
    
    // Methods
    public void display(ArrayList<Product> ProductList) throws IOException , ClassNotFoundException, FileNotFoundException{
        // read
      //Read arraylist of object from file
        try {
    
            FileInputStream fileInputStreamObj = new FileInputStream("src/Product.txt");
            ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStreamObj);
            ProductList = (ArrayList<Product>) objectInputStream.readObject();
    
            objectInputStream.close();
            objectInputStream.close();
           
        }
        catch (Exception e){
            System.out.println(e);
        }
        
        for(int i=0;i<ProductList.size();i++){
            
            Table t = new Table(5, BorderStyle.UNICODE_BOX_DOUBLE_BORDER, ShownBorders.ALL);
            t.setColumnWidth(0, 20, 40);
            t.setColumnWidth(1, 20, 40);
            t.setColumnWidth(2, 20, 40);
            t.setColumnWidth(3, 20, 40);
            t.setColumnWidth(4, 30, 50);
            if(ProductList.get(i).id==1) {
                t.addCell("ID", center);
                t.addCell("Name", center);
                t.addCell("Unit Price", center);
                t.addCell("QTY", center);
                t.addCell("Imported Date", center);
            }
            t.addCell(String.valueOf(ProductList.get(i).id), center);
            t.addCell(ProductList.get(i).name, center);
            t.addCell(String.valueOf(ProductList.get(i).stock_Q), center);
            t.addCell(String.valueOf(ProductList.get(i).stock_Q), center);
            t.addCell(ProductList.get(i).importedDate, center);
            
            System.out.print(t.render());
            
            if(i==ProductList.size()-1)
                System.out.println("\n");
        }
    
    
    }
    //Write
    public void write (ArrayList<Product> ProductList) throws IOException , ClassNotFoundException, FileNotFoundException {
        
        
        CellStyle center = new CellStyle(CellStyle.HorizontalAlign.center);
        CellStyle left = new CellStyle(CellStyle.HorizontalAlign.left);
        Table t = new Table(5, BorderStyle.UNICODE_BOX_DOUBLE_BORDER, ShownBorders.ALL);
    
        t.setColumnWidth(0, 20, 40);
        t.setColumnWidth(1, 20, 40);
        t.setColumnWidth(2, 20, 40);
        t.setColumnWidth(3, 20, 40);
        t.setColumnWidth(4, 20, 40);
    
        t.addCell("ID", center);
        t.addCell("Name", center);
        t.addCell("Unit Price", center);
        t.addCell("QTY", center);
        t.addCell("Imported Date", center);
    
        try {
        
            FileInputStream fileInputStreamObj = new FileInputStream("src/Product.txt");
            ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStreamObj);
            ProductList = (ArrayList<Product>) objectInputStream.readObject();
        
            objectInputStream.close();
            objectInputStream.close();
            System.out.println(ProductList);
        }
        catch (Exception e){
            System.out.println(e);
        }
    
        
        
//        int x;
//        if (ProductList.size()==0) {
//             x = +1;
//        } else {
//            x = ProductList.size() + 1;
//        }
//        proObj.setId(x);
//        System.out.println("Product ID : "+x);
        
        int x = ProductList.get(ProductList.size()-1).id +1;
        proObj.setId(x);
        System.out.println("Product ID        : "+x);
        t.addCell(String.valueOf(proObj.getId()), center);
    
        System.out.print("Product Name      : ");
        proName = Input.next();
        proObj.setName(proName);
        t.addCell(proObj.getName(), center);
    
        System.out.print("Product price     : ");
        proUnit = Input.nextDouble();
        proObj.setUnitPrice(proUnit);
        t.addCell(String.valueOf(proObj.getUnitPrice()), center);
    
        System.out.print("Product Qty       : ");
        proStock_Q = Input.nextInt();
        proObj.setStock_Q(proStock_Q);
        t.addCell(String.valueOf(proObj.getStock_Q()), center);
        
        proObj.setImportedDate(dateImported());
        t.addCell(proObj.getImportedDate(), center);
        
        ProductList.add(proObj);
        Table menuTable = new Table(2, BorderStyle.UNICODE_BOX_DOUBLE_BORDER_WIDE,ShownBorders.SURROUND);
    
        menuTable.setColumnWidth(0,30,50);
        menuTable.setColumnWidth(1,20,50);
    
        menuTable.addCell("ID               : ",left);
        menuTable.addCell(String.valueOf(proObj.getId()),left);
        menuTable.addCell("Name             :",left);
        menuTable.addCell(proObj.name,left);
        menuTable.addCell("Unit Price       :",left);
        menuTable.addCell(String.valueOf(proObj.unitPrice),left);
        menuTable.addCell("QTY              :",left);
        menuTable.addCell(String.valueOf(proObj.stock_Q),left);
        menuTable.addCell("Imported Date    :",left);
        menuTable.addCell(proObj.importedDate,left);
        
        System.out.println(menuTable.render());
    
        System.out.print("Do you want to Save it?\t");
        System.out.println("Press N (Cancel)/Y (Save)");
        System.out.print("=> chose option   :");
        String op = Input.next();
        String yes ="y";
        String no = "n";
        if(op.compareToIgnoreCase(yes)==0){
            save(ProductList);
            System.out.println("Successful to Save");
        }
        else {
            System.out.println(" Canceled !");
            return ;
        }
        
        try{
            FileOutputStream fileOutputStream = new FileOutputStream("src/Product.txt");
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream) ;
    
            objectOutputStream.writeObject(ProductList);
//            objectOutputStream.writeObject(ProObj1);
//            objectOutputStream.writeObject(ProObj2);
//            objectOutputStream.writeObject(ProObj3);
//
            objectOutputStream.flush();
            objectOutputStream.close();
            System.out.println("Product added Successfully");
        } catch (IOException e) {
            System.out.println("Error Initialize Stream ");
        }
    
    }
    // Search
    public static void search(ArrayList<Product> ProductList) throws IOException , ClassNotFoundException, FileNotFoundException{
        System.out.print("-->Input product's name:");
        String str = Input.next();
        
        // Jork
//        try {
//            FileInputStream file = new FileInputStream("E:\\JavaApp\\StockFile.txt");
//            ObjectInputStream is = new ObjectInputStream(file);
//            arr = (ArrayList) is.readObject();
//            is.close();
//            file.close();
//        }
//        catch (Exception e){
//            System.out.println(e);
//        }
    
        // read Thary
        
        // 1st Method to read
        
       
        
        // 2nd Method to read
        
//        FileInputStream fileInputStreamObj = new FileInputStream("src/Product.txt");
//        boolean cont = true;
//        while (cont) {
//            try {
//                ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStreamObj);
//                Object obj = objectInputStream.readObject();
//                if (obj != null){
//                    // ProductList.add((Product) obj);
//                    System.out.println(obj);
//                } else {
//                    cont = false;
//                }
//                //System.out.println(ProductList);
//                objectInputStream.close();
//                fileInputStreamObj.close();
//            } catch (FileNotFoundException e) {
//                System.out.println("File not fond");
//            } catch (IOException e) {
//                System.out.println("Error Initialize ");
//                break;
//            } catch ( Exception e) {
//                e.printStackTrace();
//            }
//        }
    
        try{
        
            FileInputStream fileInputStreamObj = new FileInputStream("src/Product.txt");
            ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStreamObj);
            Object obj = objectInputStream.readObject();
            System.out.println(obj);
        
            objectInputStream.close();
            fileInputStreamObj.close();
        
        } catch (Exception e) {
            e.printStackTrace();
        }
//
        
        for(int i= 0; i<ProductList.size();i++){
            if(str.compareToIgnoreCase(ProductList.get(i).name)==0){
                CellStyle center = new CellStyle(CellStyle.HorizontalAlign.center);
                Table menuTable = new Table(9, BorderStyle.UNICODE_BOX_DOUBLE_BORDER_WIDE,ShownBorders.SURROUND);
            
                menuTable.setColumnWidth(0,15,50);
                menuTable.setColumnWidth(1,15,50);
                menuTable.setColumnWidth(2,15,50);
                menuTable.setColumnWidth(3,15,50);
                menuTable.setColumnWidth(4,15,50);
            
                menuTable.addCell(String.valueOf(ProductList.get(i).id),center);
                menuTable.addCell(ProductList.get(i).name,center);
                menuTable.addCell(String.valueOf(ProductList.get(i).unitPrice),center);
                menuTable.addCell(String.valueOf(ProductList.get(i).stock_Q),center);
                menuTable.addCell(ProductList.get(i).importedDate,center);
                System.out.println(menuTable.render());
            }
            else if(i== ProductList.size()-1) {
                    System.out.println("-->>Can not found this name!!!!!");
            }
        }
    
    }
    // Read by ID
    public static void read(ArrayList<Product> ProductList ){
        
        System.out.print("=> Input product's ID:");
        String str = Input.next();
        int x = Integer.parseInt(str);
        try {
            FileInputStream fileInputStreamObj = new FileInputStream("src/Product.txt");
            ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStreamObj);
           // arr = (ArrayList) is.readObject();
            ProductList = (ArrayList<Product>) objectInputStream.readObject();
            objectInputStream.close();
            fileInputStreamObj.close();
        }
        catch (Exception e){
            System.out.println(e);
        }
        for(int i= 0; i<ProductList.size();i++){
            if(x==(ProductList.get(i).id)){
                CellStyle center = new CellStyle(CellStyle.HorizontalAlign.center);
                Table menuTable = new Table(9, BorderStyle.UNICODE_BOX_DOUBLE_BORDER_WIDE,ShownBorders.SURROUND);
            
                menuTable.setColumnWidth(0,15,50);
                menuTable.setColumnWidth(1,15,50);
                menuTable.setColumnWidth(2,15,50);
                menuTable.setColumnWidth(3,15,50);
                menuTable.setColumnWidth(4,15,50);
            
                menuTable.addCell(String.valueOf(ProductList.get(i).id),center);
                menuTable.addCell(ProductList.get(i).name,center);
                menuTable.addCell(String.valueOf(ProductList.get(i).unitPrice),center);
                menuTable.addCell(String.valueOf(ProductList.get(i).stock_Q),center);
                menuTable.addCell(ProductList.get(i).importedDate,center);
                System.out.println(menuTable.render());
            }
            else if(i== ProductList.size()-1) {
                    System.out.println("-->>Can not found this ID!!!!!");
            }
        }
    
    }
    // Delete by ID
    public static void delete(ArrayList<Product>ProductList){
        System.out.print("=> Input Product's ID to delete:");
        String str = Input.next();
        int x= Integer.parseInt(str);
        try {
            FileInputStream fileInputStreamObj = new FileInputStream("src/Product.txt");
            ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStreamObj);
            ProductList = (ArrayList<Product>) objectInputStream.readObject();
            objectInputStream.close();
            fileInputStreamObj.close();
        }
        catch (Exception e){
            System.out.println(e);
        }
        for(int i= 0; i<ProductList.size();i++){
            if(x==(ProductList.get(i).id)){
                CellStyle center = new CellStyle(CellStyle.HorizontalAlign.center);
                Table menuTable = new Table(9, BorderStyle.UNICODE_BOX_DOUBLE_BORDER_WIDE,ShownBorders.SURROUND);
            
                menuTable.setColumnWidth(0,15,50);
                menuTable.setColumnWidth(1,15,50);
                menuTable.setColumnWidth(2,15,50);
                menuTable.setColumnWidth(3,15,50);
                menuTable.setColumnWidth(4,15,50);
            
                menuTable.addCell(String.valueOf(ProductList.get(i).id),center);
                menuTable.addCell(ProductList.get(i).name,center);
                menuTable.addCell(String.valueOf(ProductList.get(i).unitPrice),center);
                menuTable.addCell(String.valueOf(ProductList.get(i).stock_Q),center);
                menuTable.addCell(ProductList.get(i).importedDate,center);
                System.out.println(menuTable.render());
                
                System.out.print("Do you want to delete it?\t");
                System.out.println("Press N to cancel / Press Y to delete");
                System.out.print("-->chose option:");
                String op = Input.next();
                String  yes ="y";
                String no = "n";
                if(op.compareToIgnoreCase(yes)==0){
                    ProductList.remove(i);
                    save(ProductList);
                    System.out.println("Successful to delete");
                }
                else return;
            }
            else if(i== ProductList.size()-1) {
                    System.out.println("-->>Can not found this ID!!!!!");
            }
        }
    
    }
    // Save
     public static void save(ArrayList<Product> ProductList){
         //Write arraylist to file
         try {
             FileOutputStream fileOutputStream = new FileOutputStream("src/Product.txt");
             ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream) ;
             objectOutputStream.writeObject(ProductList);
             
             objectOutputStream.close();
             fileOutputStream.close();
         }
         catch (IOException ioe) {
             ioe.printStackTrace();
         }
     }
    // Update
   // Boran Jork, [1 May 2021 at 14:56:59]:
     public static void update(ArrayList <Product> ProductList){
         System.out.print("Input Product iD :");
         String pro= Input.next();
         int x = Integer.parseInt(pro);
         try {
             FileInputStream fileInputStreamObj = new FileInputStream("src/Product.txt");
             ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStreamObj);
             // arr = (ArrayList) is.readObject();
             ProductList = (ArrayList<Product>) objectInputStream.readObject();
             objectInputStream.close();
             fileInputStreamObj.close();
         }
         catch (Exception e){
             System.out.println(e);
         }
         for(int i= 0; i<ProductList.size();i++){
             if(x==(ProductList.get(i).id)){
                 CellStyle center = new CellStyle(CellStyle.HorizontalAlign.center);
                 Table menuTable = new Table(9, BorderStyle.UNICODE_BOX_DOUBLE_BORDER_WIDE,ShownBorders.SURROUND);
                
                 menuTable.setColumnWidth(0,15,50);
                 menuTable.setColumnWidth(1,15,50);
                 menuTable.setColumnWidth(2,15,50);
                 menuTable.setColumnWidth(3,15,50);
                 menuTable.setColumnWidth(4,15,50);
                
                 menuTable.addCell(String.valueOf(ProductList.get(i).id),center);
                 menuTable.addCell(ProductList.get(i).name,center);
                 menuTable.addCell(String.valueOf(ProductList.get(i).unitPrice),center);
                 menuTable.addCell(String.valueOf(ProductList.get(i).stock_Q),center);
                 menuTable.addCell(ProductList.get(i).importedDate,center);
                 System.out.println(menuTable.render());
                 
                 System.out.println("*******************************************");
                 Table menuTable1 = new Table(9, BorderStyle.UNICODE_BOX_DOUBLE_BORDER_WIDE,ShownBorders.SURROUND);
                
                 menuTable1.setColumnWidth(0,15,50);
                 menuTable1.setColumnWidth(1,15,50);
                 menuTable1.setColumnWidth(2,15,50);
                 menuTable1.setColumnWidth(3,15,50);
                 menuTable1.setColumnWidth(4,15,50);
                
                 menuTable1.addCell("1.Update all ",center);
                 menuTable1.addCell("2.Update name ",center);
                 menuTable1.addCell("3.Update UnitPrice",center);
                 menuTable1.addCell("4.Update stock_Qty",center);
                 menuTable1.addCell("5.Back to menu",center);
                 System.out.println(menuTable1.render());
                 
                 System.out.print("-->Chose option:");
                 String str1 = Input.next();
                 int y = Integer.parseInt(str1);
                 switch (y){
                     case 1 -> {
                         System.out.print("-->Update new ID:");
                         String text = Input.next();
                         int id = Integer.parseInt(text);
                         System.out.print("-->Update new name:");
                         String name = Input.next();
                         System.out.print("-->Update new unitPrice:");
                         double pri = Input.nextDouble();
                         System.out.print("-->Update new Qty:");
                         String qty = Input.next();
                         int Qty = Integer.parseInt(qty);
                         ProductList.get(i).id = id;
                         ProductList.get(i).name =name;
                         ProductList.get(i).unitPrice=pri;
                         ProductList.get(i).stock_Q=Qty;
                         save(ProductList);
                         
                     }
                     
                     case 2 -> {
                         System.out.print("-->Update new name:");
                         String name1 = Input.next();
                         ProductList.get(i).name =name1;
                         save(ProductList);
                         
                     }
                     
                     case 3 -> {
                         System.out.print("-->Update new unitPrice:");
                         double pri1 = Input.nextDouble();
                         ProductList.get(i).unitPrice=pri1;
                         save(ProductList);
                         
                     }
                     
                     case 4 -> {
                         System.out.print("-->Update new Qty:");
                         String qty1 = Input.next();
                         int Qty1 = Integer.parseInt(qty1);
                         ProductList.get(i).stock_Q=Qty1;
                         save(ProductList);
                         
                     }
                        
                     
                     case 5 -> {
                         return;
                     }
                     
                     default -> {
                         System.out.println("--------Input Invalid----------");
                         return;
                     }
                     
                 }
                
                
             } else if(i== ProductList.size()-1){
                 
                     System.out.println("-->>Can not found this ID!!!!!");
             }
         }
        
     }
     
    // Main
         public static void main (String[]args) throws IOException, ClassNotFoundException {
            Loading loadObj = new Loading();
            Main mainObj = new Main();
            do {
                // Welcome Screen
                Table t = new Table(1, BorderStyle.UNICODE_BOX_DOUBLE_BORDER, ShownBorders.NONE);
                t.setColumnWidth(0, 15, 30);
                t.addCell("Welcome to Stock Management");
                System.out.println(t.render());

//                loadObj.start();
//                try {
//                    loadObj.join();
//                } catch (Exception e) {
//                    System.out.println(e);
//                }
//                mainObj.groupLogo();
//                mainObj.menuOption();
    
                Product ProObj1 = new Product(1,"Coca",3,10,"2021-03-03");
                Product ProObj2 = new Product(2,"Pepsi",5,15,"2021-03-03");
                Product ProObj3 = new Product(3,"Sting",8,20,"2021-03-03");
    
                ProductList.add(ProObj1);
                ProductList.add(ProObj2);
                ProductList.add(ProObj3);
                
                System.out.print("Command --> ");
                int option = Input.nextInt();
                switch (option) {
                    case 1 -> {
                        mainObj.display(ProductList);
                    }
                    case 2 -> {
                        mainObj.write(ProductList);
                    }
                    case 3 -> {
                        // Read by ID
                       read(ProductList);
                    }
                    
                    case 4 -> {
                        // Search by Name
                        
                        search(ProductList);
                    }
                    case 5 -> {
                        // Delete by ID
                        delete(ProductList);
                    }
                    case 6 -> {
                        update(ProductList);
                    }
                }
            } while (true);
        }
}
