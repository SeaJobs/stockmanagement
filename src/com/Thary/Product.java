package com.Thary;

import org.nocrala.tools.texttablefmt.BorderStyle;
import org.nocrala.tools.texttablefmt.CellStyle;
import org.nocrala.tools.texttablefmt.ShownBorders;
import org.nocrala.tools.texttablefmt.Table;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class Product implements Serializable {
	//private int count;
	protected int id ;
	protected String name ;
	protected double unitPrice ;
	protected int stock_Q;
	protected String importedDate ;
	
	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public double getUnitPrice() {
		return unitPrice;
	}
	
	public void setUnitPrice(double unitPrice) {
		this.unitPrice = unitPrice;
	}
	
	public int getStock_Q() {
		return stock_Q;
	}
	
	public void setStock_Q(int stock_Q) {
		this.stock_Q = stock_Q;
	}
	
	
//	public static String dateImported () {
//
//		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd");
//		LocalDateTime now = LocalDateTime.now();
//
//		return dtf.format(now);
//	}
	
	public String getImportedDate() {
		return importedDate;
	}

	public void setImportedDate(String importedDate) {
		this.importedDate = importedDate;
	}
	
	public Product(int id, String name, double unitPrice, int stock_Q,String importedDate) {
		this.id = id;
		this.name = name;
		this.unitPrice = unitPrice;
		this.stock_Q = stock_Q;
		this.importedDate = importedDate;
	}
	
//	@Override
//	public String toString() {
//
//		CellStyle center = new CellStyle(CellStyle.HorizontalAlign.center);
//		Table t = new Table(5, BorderStyle.UNICODE_BOX_DOUBLE_BORDER,ShownBorders.ALL);
//
//		t.setColumnWidth(0, 20, 40);
//		t.setColumnWidth(1, 20, 40);
//		t.setColumnWidth(2, 20, 40);
//		t.setColumnWidth(3, 20, 40);
//		t.setColumnWidth(4, 20, 40);
//
//		t.addCell("ID",center);
//		t.addCell("Name",center);
//		t.addCell("Unit Price",center);
//		t.addCell("QTY",center);
//		t.addCell("Imported Date",center);
//
//		t.addCell(String.valueOf(id),center);
//		t.addCell(name,center);
//		t.addCell(String.valueOf(unitPrice),center);
//		t.addCell(String.valueOf(stock_Q),center);
//		t.addCell(importedDate,center);
//
//		return t.render();
//	}
}
